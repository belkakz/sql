<?php 
$pdo = new PDO("mysql:host=localhost;dbname=global;charset=utf8", "shesterin", "neto1766");
// $sql = "SELECT * FROM books";
	$sql = "SELECT * FROM books";
$isbn = FALSE;
$bookName = FALSE;
$author = FALSE;
if (!empty($_GET['isbn'])) {
	$isbn = $_GET['isbn'];
	$sql = $sql." WHERE isbn LIKE '%$isbn%'";
}
if (!empty($_GET['bookName'])) {
	$bookName = $_GET['bookName'];
	if (!$isbn) {
		$sql = $sql." WHERE name LIKE '%$bookName%'";
	}
	else $sql = $sql." AND name LIKE '%$bookName%'";
}
if (!empty($_GET['author'])) {
	$author = $_GET['author'];
	if (($isbn) || ($bookName)) {
		$sql = $sql." AND author LIKE '%$author%'";
	} else $sql = $sql." WHERE author LIKE '%$author%'";
}
$newBase = [];
foreach ($pdo->query($sql) as $row) {
	$newBase[] = $row;
}
function tablePrint($newBase) {
		    	foreach ($newBase as $key => $value) 
	{
		echo '<tr>';
				echo '<td>'.$value['name'].'</td>';    				
				echo '<td>'.$value['author'].'</td>'; 				
				echo '<td>'.$value['year'].'</td>';			
				echo '<td>'.$value['genre'].'</td>';			
				echo '<td>'.$value['isbn'].'</td>';
		echo '</tr>';
	}
};
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Библиотека</title>
	<style>
    table { 
        border-spacing: 0;
        border-collapse: collapse;
    }
    table td, table th {
        border: 1px solid #ccc;
        padding: 5px;
    }
    
    table th {
        background: #eee;
    }
	</style>
</head>
<body>
	


	<h1>Библиотека успешного человека</h1>
	<form action="index.php" method="GET" style="margin-bottom: 20px">
		<input type="text" name="isbn" placeholder="ISBN" value="<?php echo $isbn; ?>">
		<input type="text" name="bookName" placeholder="Название книги" value="<?php echo $bookName; ?>">
		<input type="text" name="author" placeholder="Автор" value="<?php echo $author; ?>">
		<input type="submit" value="Поиск">
	</form>
	<form action="index.php" method="GET">
		<input type="submit" value="Сбросить">
	</form>
	<table>
	    <tbody>
	    	<tr>
		        <th>Название</th>
		        <th>Автор</th>
		        <th>Год выпуска</th>
		        <th>Жанр</th>
		        <th>ISBN</th>
	    	</tr>
	    	<?php 
	    		tablePrint($newBase);
			?>
	</tbody>
	</table>
</body>
</html>